<?php
/**
 * @file
 * Provides CCK integration for simplestar module
 */

/**
 * Implementation of hook_field_info().
 */
function simplestar_field_info() {
  return array(
    'simplestar' => array(
      'label' => t('Simplestar'),
      'description' => t('Simplestar rating widget.'),
    ),
  );
}

/**
 * Implementation of hook_field_settings().
 */
function simplestar_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['stars'] = array(
        '#type' => 'textfield',
        '#title' => t('Stars'),
        '#default_value' => intval($field['stars'])>0 ? intval($field['stars']) : 5,
        '#required' => FALSE,
        '#element_validate' => array('_simplestar_star_value_validate'),
        '#description' => t('Number of stars.'),
      );
      return $form;
      
    case 'save':
      return array('stars');

    case 'database columns':
      return array(
        'simplestar' => array('type' => 'int', 'not null' => FALSE, 'sortable' => TRUE),
      );
  }
}

/**
 * Validate the stars field element 
 */
function _simplestar_star_value_validate($element, &$form_state) {
  $value = $form_state['values']['stars'];
  if ($value != '' && (!is_numeric($value)  || intval($value) < 2)) {
    form_set_error('stars', t('Please enter a valid number of stars value.'));
  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function simplestar_content_is_empty($item, $field) {
  if (empty($item['simplestar'])) {
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Implementation of hook content_generate().
 */
function simplestar_content_generate($node, $field) {
  $node_field = array();
  $node_field['value'] = rand(1, $field['stars']);
  
  return $node_field;
}

/**
 * Implementation of hook_widget_info().
 */
function simplestar_widget_info() {
  return array(
    'simplestar' => array(
      'label' => t('Simplestar'),
      'field types' => array('simplestar'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_DEFAULT,
      ),
    )
  );
}

/**
 * Implementation of hook_widget_settings().
 */
function simplestar_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['cancelshow'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show cancel'),
        '#default_value' => $widget['cancelshow'],
        '#description' => t('Show the cancel option.'),
      );
      $form['onevoteonly'] = array(
        '#type' => 'checkbox',
        '#title' => t('One vote only'),
        '#default_value' => $widget['onevoteonly'],
        '#description' => t('Allow only one vote.'),
      );
      $form['split'] = array(
        '#type' => 'textfield',
        '#title' => t('Split'),
        '#default_value' => intval($widget['split']),
        '#description' => t('Number of splits for stars (\'0\' for no split).'),
        '#element_validate' => '_simplestar_widget_settings_split_validate',
      );
      
      return $form;
    case 'save':
      return array('cancelshow', 'onevoteonly', 'split');
  }
}

function _simplestar_widget_settings_split_validate($element, &$form_state) {
  $value = $form_state['values']['split'];
  
  if ($value) {
    if (!is_numeric($value)) {
      form_set_error('min', t('"Split" must be a number.'));
    }
    elseif ($value < 0) {
      form_set_error('min', t('"Split" must be greater than zero.'));
    }
  }
}

/**
 * Implementation of hook_widget().
 */
function simplestar_widget(&$form, &$form_state, $field, $items) {
  $element = array(
    '#type' => 'simplestar',
    '#default_value' => isset($items[0]['simplestar']) ? $items[0]['simplestar'] : NULL,
    '#stars' => is_numeric($field['stars']) ? $field['stars'] : 5,
    '#onevoteonly' => $field['widget']['onevoteonly'],
    '#cancelshow' => $field['widget']['cancelshow'],
    '#split' => intval($field['widget']['split']),
  );
  
  return $element;
}

/**
 * Implementation of hook_field_formatter_info().
 */
function simplestar_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('Stars'),
      'field types' => array('simplestar'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'rating' => array(
      'label' => t('Rating Value'),
      'field types' => array('simplestar'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Theme function for 'default' simplestar field formatter.
 */
function theme_simplestar_formatter_default($element) {
  if (empty($element['#item']['simplestar'])) {
    $element['#item']['simplestar'] = 0;
  }

  module_load_include('inc', 'content', 'includes/content.crud');
  $item = content_field_instance_read(array('type_name' => $element['#type_name'], 'field_name' => $element['#field_name']), FALSE);
  $simplestar = $item[0]['widget']['default_value'][0];

  $settings = array(
    '#title' => $element['#title'],
    '#description' => $element['#description'],
    '#field_name' => $element['#field_name'],
    '#stars' => $simplestar['#post']['stars'],
    '#split' => intval($simplestar['#post']['split']),
  );

  return simplestar_static($element['#item']['simplestar'], $settings);
}

/**
 * Theme function for 'rating' simplestar field formatter.
 */
function theme_simplestar_formatter_rating($element) {
  if (!isset($element['#item']['simplestar'])) {
    $element['#item']['simplestar'] = 0;
  }

  return $element['#item']['simplestar'];
}
