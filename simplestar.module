<?php
/**
 * @file
 * Implements simplestar field element.
 */

/**
 * Implementation of hook_help(). 
 */
function simplestar_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#simplestar':
      $output = t('A simple star rating widget based on jQueryUI Star rating (<a href="@link">@link</a>)', array('@link' => 'http://orkans-tmp.22web.net/star_rating'));
      $steps = array(
        t('To enable rating for content types, follow below steps.'),
        t('Go to <a href="@types">admin/content/types</a> and select manage fields.', array('@types' => url('admin/content/types'))),
        t('Add a simplestar rating field for the content type you want to rate, on the field setting step will be able to configure the star rating widget.'),
      );
      $output .= theme('item_list', $steps, NULL, 'ol');
      
      return $output;
  }
}

/**
 * Implementation of hook_init().
 */
function simplestar_init() {
  if (module_exists('content')) {
    module_load_include('inc', 'simplestar', 'simplestar_field');
  }
}

/**
 * Implementation of hook_elements().
 */
function simplestar_elements() {
  $type['simplestar'] = array(
    '#input' => TRUE,
    '#stars' => 5,
    '#inputtype' => 'radios',
    '#disabled' => FALSE,
    '#split' => FALSE,
    '#onevoteonly' => FALSE,
    '#cancelshow' => FALSE,
    '#process' => array('_simplestar_field_process'),
    '#element_validate' => array('_simplstar_field_validate'),
  );
  
  return $type;
}

/**
 * Theme function for simplestar element 
 */
function theme_simplestar($element) {
  $field_name = $element['#field_name']!='' ? $element['#field_name'] : 'simplestar';
  $path = drupal_get_path('module', 'simplestar') .'/jquery.ui.stars';
  
  jquery_ui_add('ui.widget');
  drupal_add_css($path .'/jquery.ui.stars.min.css', 'module');
  drupal_add_js($path .'/jquery.ui.stars.min.js', 'module');
  drupal_add_js(
    'Drupal.behaviors.'. $field_name .' = function() {
       $(".'. $field_name .'").children().not(":input").hide();
       $(".'. $field_name .'").stars(
	     '. _simplestar_get_settings($element) .'
	   );
	 };',
    'inline', 'footer');
  
  return theme('form_element', $element, '<div class="'. $field_name .'"><div class="container-inline">'. $element['#children'] .'</div></div><div class="clear-block"></div>');
}

/**
 * Implementation of hook_theme().
 */
function simplestar_theme() {
  return array(
    'simplestar' => array(
       'arguments' => array('element' => NULL),
    ),
    'simplestar_formatter_default' => array(
       'arguments' => array('element' => NULL),
    ),
    'simplestar_formatter_rating' => array(
      'arguments' => array('element' => NULL),
    ),
  );
}

/**
 * Implementation of form_type_hook_value().
 */
function form_type_simplestar_value($element, $edit = FALSE) {
  if ($edit === FALSE) {
    return $element;
  }
  
  return $edit;
}

/**
 * Render a simplestar static element
 * @param $rating
 *        Rating value (eg: 1,2,3..etc)
 * @param $settings
 * 		  Specify settings (eg: array('#description' => '', '#stars' => 5))
 */
function simplestar_static($rating, $settings=FALSE) {
  $element['simplestar']['#field_name'] = 'radios_'. $settings['#field_name'];
  $element['simplestar']['#title'] = $settings['#title'];
  $element['simplestar']['#description'] = $settings['#description'];
  $element['simplestar']['#split'] = $settings['#split'];
  $element['simplestar']['#disabled'] = TRUE;
  
  for ($i=1; $i<=$settings['#stars']; $i++) {
    $radio = array(
      '#type' => 'radio',
      '#id' => 'star-'. $i,
      '#name' => $settings['#field_name'],
      '#return_value' => $i,
      '#value' => intval($rating),
      '#parents' => array(),
    );
    $element['simplestar']['#children'] .= theme('radio', $radio);
  }
  $element['simplestar']['#children'] = theme('radios', $element['simplestar']);

  return theme('simplestar', $element['simplestar']);
}

/**
 * Process simplestar element 
 */
function _simplestar_field_process($element, $edit=NULL, $form_state, $form) {
  $field = _simplestar_get_fieldname($element);
  $type = $element['#inputtype']=='select'?'select':'radios';
  
  $element[$field] = array(
    '#type' => $type,
    '#default_value' => $element['#default_value']?$element['#default_value']:($edit?$edit['#default_value']:''),
    '#options' => drupal_map_assoc(range(1, $element['#stars'])),
  );

  return $element;
}

/**
 * Validate simplestar element  
 */
function _simplstar_field_validate($element) {
  $field = _simplestar_get_fieldname($element);
  
  if ($element['#required'] && !isset($element['#post'][$field])) {
    form_set_error($field, t('Required value'));
  }
}

/**
 * Get field name from the element
 */
function _simplestar_get_fieldname($element) {
  $field = end($element['#array_parents']);
  $field = $field?$field:'simplestar';
  
  return $field;
}

/**
 * Get jQueryUI star settings
 */
function _simplestar_get_settings($element) {
  $settings = array();
  
  if ($element['#inputtype'] == 'select') {
    $settings['inputType'] = 'select';
  }
  if ($element['#disabled']) {
    $settings['disabled'] = TRUE;
  }
  if (intval($element['#split']) > 0) {
    $settings['split'] = $element['#split'];
  }
  if ($element['#onevoteonly']) {
    $settings['oneVoteOnly'] = TRUE;
  }
  $settings['cancelShow'] = $element['#cancelshow'] ? TRUE : FALSE;
  
  return drupal_to_js($settings);
}