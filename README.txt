Simplestar
-----------
A simple star rating widget based on jQueryUI Star rating (http://orkans-tmp.22web.net/star_rating). 
Comes with CCK integration to provide a CCK simplestar rating widget.
This module requires jQueryUI module.

Features
---------
- Themable stars based on jQueryUI Star rating widget 
- Configuration options such as split stars, cancel button..etc
- Graceful degradation to radio/select when javascript disabled
- Easy to use with Form API in other modules
- A Simplestar CCK field for use in custom node types


Installation
--------------
1. Download and install jQueryUI module from http://drupal.org/project/jquery_ui
2. Download and place jQueryUI Star rating widget in the folder 'jquery.ui.stars' from http://orkans-tmp.22web.net/star_rating/#download
3. Enable the module simplestar